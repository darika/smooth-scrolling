import env from '../utils/ENV';
import Signal from '../classes/Signal';

const htmlClassList = document.documentElement.classList;
let SCROLLBAR_WIDTH;

(() => {
	const initialStyle = document.body.style.overflow;
	document.body.style.overflow = 'hidden';
	const fullWidth = document.body.offsetWidth;
	document.body.style.overflow = 'visible';
	document.body.style.height = '110%';
	const scrollWidth = document.body.offsetWidth;
	document.body.style.overflow = initialStyle;
	document.body.style.height = '';
	SCROLLBAR_WIDTH = fullWidth - scrollWidth;
	document.documentElement.style.setProperty('--scrollbar-size', SCROLLBAR_WIDTH + 'px');
	htmlClassList.add('_scrollbar-size-detected');
})();

class PageScrolling {
	constructor() {
		this.mobileMode = env.isMobile || env.isIE || env.isEdge;

		this.scrollTop = window.pageYOffset;
		this.onScroll = this.onChange = new Signal();

		if (this.mobileMode) {
			document.documentElement.classList.add('_direct-scrolling');
		} else {
			document.documentElement.classList.add('_smooth-scrolling');
		}
	}

	_manageDocumentClass() {
		this.scrollTop > 0 ? htmlClassList.add('_scrolled') : htmlClassList.remove('_scrolled');
	}

	start() {
		this.started = true;
		if (this.mobileMode) {
			window.addEventListener('scroll', () => {
				const scrollTop = window.pageYOffset;
				this.scrollTop = scrollTop < 0 ? 0 : scrollTop;
				this._manageDocumentClass();
				this.onScroll.call(this.scrollTop);
			});
		} else {
			this.wrapper = document.querySelector('.wrapper');
			this.content = this.wrapper.querySelector('.wrapper__content');
			this.section = this.content.querySelectorAll('.section');

			this.creator = document.createElement('div');
			this.creator.className = 'scroll-creator';

			gsap.set(this.content, { force3D: true });
			gsap.set(this.section, { force3D: true });

			let smoothedScrollTop = window.pageYOffset;
			let scrollTop = smoothedScrollTop;
			let scrollSkewY = 0;
			let contentHeight;

			document.body.appendChild(this.creator);

			const updatePosition = forced => {
				if (this.forcedScroll) {
					forced = true;
					smoothedScrollTop = scrollTop;
				}
				this.forcedScroll = false;

				if (smoothedScrollTop !== scrollTop) {
					smoothedScrollTop += (scrollTop - smoothedScrollTop) / 10;
					scrollSkewY = (scrollTop - smoothedScrollTop) / 100;
				} else {
					if (!forced) {
						return;
					}
				}

				if (Math.abs(smoothedScrollTop - scrollTop) < 0.5) {
					smoothedScrollTop = scrollTop;
				}

				gsap.set(this.section, { skewY: scrollSkewY });
				gsap.set(this.content, { y: -smoothedScrollTop });

				this.scrollTop = smoothedScrollTop;
				this._manageDocumentClass();

				this.onScroll.call(Math.round(smoothedScrollTop));
			};

			const resizeHandler = () => {
				contentHeight = this.content.getBoundingClientRect().height;
				const maxScrollTop = contentHeight - window.innerHeight;
				if (smoothedScrollTop > maxScrollTop) {
					smoothedScrollTop = maxScrollTop;
					updatePosition(true);
				}
				this.creator.style.height = contentHeight + 'px';
			};
			window.addEventListener('resize', resizeHandler);
			resizeHandler();

			window.addEventListener('scroll', () => {
				scrollTop = window.pageYOffset;
				scrollTop = scrollTop < 0 ? 0 : scrollTop;
			});

			gsap.ticker.add(() => {
				updatePosition();
			});
		}
	}
}

export default new PageScrolling();
